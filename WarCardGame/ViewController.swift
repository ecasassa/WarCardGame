//
//  ViewController.swift
//  WarCardGame
//
//  Created by Etienne Casassa on 6/24/19.
//  Copyright © 2019 Etienne Casassa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    //connecting the code to the card image
    @IBOutlet weak var leftCardView: UIImageView!
    
    @IBOutlet weak var rightCardView: UIImageView!
    
    //connecting the code to the scores
    @IBOutlet weak var leftScoreLabel: UILabel!
    
    @IBOutlet weak var rightScoreLabel: UILabel!
    
    //stating the initial scores
    var leftScore = 0
    
    var rightScore = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func dealTapped(_ sender: Any) {
    
        //randomize some numbers
        let leftNumber = Int.random(in: 2...14)
        let rightNumber = Int.random(in: 2...14)
        
        //draw new card
        leftCardView.image = UIImage(named: "card\(leftNumber)")
        rightCardView.image = UIImage(named: "card\(rightNumber)")
        
        if leftNumber > rightNumber {
            //left player wins
            leftScore += 1
            leftScoreLabel.text = String(leftScore)
        }
        else if rightNumber > leftNumber {
            //right player wins
            rightScore += 1
            rightScoreLabel.text = String(rightScore)
        }
        else {
            //if there's a tie
        }
    }
    
}
